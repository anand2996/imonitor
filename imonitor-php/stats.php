<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Indoor Monitor</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="starter-template.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/ie-emulation-modes-warning.js"></script>
    <script src="amcharts/amcharts.js" type="text/javascript"></script>
    <script src="amcharts/serial.js" type="text/javascript"></script>
    <script src="http://www.amcharts.com/lib/3/plugins/dataloader/dataloader.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    var chart;
	//var chartData = [];
	var chartCursor;

    AmCharts.ready(function() {
        // chart code will go here
        chart = AmCharts.makeChart( "chartdiv", {
  "type": "serial",
  "dataLoader": {
    "url": "data.php"
  },
  "pathToImages": "amcharts/images/",
  "categoryField": "timeStamp",
  "dataDateFormat": "YYYY-MM-DD JJ:NN:SS",
  "rotate": false,
  "categoryAxis": {
    "parseDates": true,
    "minPeriod" : "mm",
    "labelRotation" : 45,
    "minorGridEnabled": true
  },
  "chartScrollbar":{
    "graph": "g1",
    "scrollbarHeight": 40,
    "color": "#FFFFFF",
    "autoGridCount": true
  },
  "chartCursor":{
    "categoryBalloonDateFormat": "MMM DD, L:NN A",
    "cursorPosition": "mouse"
  },
  "graphs": [ {
    "id": "g1",
    "valueField": "temp",
    "balloonText": "<div style='margin:5px; font-size:12px;'>Temp:<b>[[value]] Degrees</b></div>",
    "bullet": "round",
    "bulletBorderColor": "#FFFFFF",
    "bulletBorderThickness": 2,
    "lineThickness ": 2,
    "lineAlpha": 0.5
  }, {
    "valueField": "humidity",
    "balloonText": "<div style='margin:5px; font-size:12px;'>Humidity:<b>[[value]]%</b></div>",
    "bullet": "round",
    "bulletBorderColor": "#FFFFFF",
    "bulletBorderThickness": 2,
    "lineThickness ": 2,
    "lineAlpha": 0.5
  } ]
} );
chart.addListener("dataUpdated",zoomChart);
zoomChart();
function zoomChart(){
    chart.zoomToIndexes(chart.chartData.length - 72, chart.chartData.length - 1);
}
    });

    </script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Plant Monitoring</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php">Current Climate</a></li>
            <li class="active"><a href="stats.php">Statistics</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
    <div class="container">
      <div class="starter-template">
        <h1>Statistics</h1>
      	<p class="lead">
            <div id="chartdiv" class="img-responsive center-block" style="width: 100%; height: 500px;"></div>
        </p>
      </div>

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

    <script type="text/javascript">

    </script>
  </body>
</html>

