# -*- coding: utf-8 -*-
import time
from IMTemp import IMTemp
from IMCamera import IMCamera
from IMMessage import IMMessage

#VARIABLES
pollingInterval = 600  #In seconds
TEMP_High = 85
TEMP_Low = 70
SMS_Notifications = False
imMessage = IMMessage(SMS_Notifications)
imTemp = IMTemp(imMessage, TEMP_High, TEMP_Low)
imCamera = IMCamera()
log = open('/home/pi/Desktop/Python Programming/plant-monitor/logs/IMLog.txt', 'a')


#FUNCTIONS


#LOOP
while True:
    print("-------------------------------------")
    print(time.strftime("%m/%d/%Y %I:%M:%S %p") + " - Indoor Monitoring Started")
    log.write("\n-------------------------------------\n" + time.strftime("%m/%d/%Y %I:%M:%S %p") + " - Plant Monitor Started")
    log.flush()
    imTemp.getTemp()
    imCamera.takePic()
    print(time.strftime("%m/%d/%Y %I:%M:%S %p") + " - Indoor Monitoring Completed")
    log.write("\n-------------------------------------\n" + time.strftime("%m/%d/%Y %I:%M:%S %p") + " - Plant Monitor Completed")
    log.flush()
    time.sleep(pollingInterval)
